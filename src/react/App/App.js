import {Provider} from "react-redux";
import React, { Component } from 'react';
import {Route, Switch, BrowserRouter} from 'react-router-dom';

import store from "../../redux/store";
import './App.css';
import Scores from "../Scores";
import Layout from "../Layout/Layout";
import Rules from "../Rules/Rules";

import Home from "../Home/Home";
import Board from "../Board/Board";
import HighScores from "../HighScores/HighScores";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Layout>
          <BrowserRouter>
            <Switch>
              <Route
                exact
                path="/board"
                component={Board}
              />
              <Route
                exact
                path="/rules"
                component={Rules}
              />
              <Route
                exact
                path="/high"
                component={HighScores}
              />
              <Route
                exact
                path="/"
                component={Home}
              />
            </Switch>
          </BrowserRouter>
        </Layout>
      </Provider>
    );
  }
}

export default App;
