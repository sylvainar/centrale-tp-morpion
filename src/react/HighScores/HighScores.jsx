import React from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {getHighScores} from "../../redux/scores/actions";

class HighScores extends React.PureComponent {
  static propTypes = {
    fetchHighScores: PropTypes.func.isRequired,
    loading: PropTypes.bool,
  };

  static defaultProps = {
    loading: false,
  };

  componentDidMount() {
    this.props.fetchHighScores();
  }

  render() {
    return (
      <div>
        <h1>HighScores</h1>
        {this.props.loading && <p>Loading</p>}
        <p>{JSON.stringify(this.props.highScores)}</p>
      </div>
    );
  }
}

export default connect(state => ({
  highScores: state.scores.highScores,
  loading: state.scores.loading,
}), dispatch => ({
  fetchHighScores: () => dispatch(getHighScores()),
}))(HighScores);
