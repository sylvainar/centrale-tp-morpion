import React from 'react';
import {hasWon, INITIAL_GRID, INITIAL_PLAYER, nextPlayer} from "../../services/morpion";

const initialState =  {
  grid: INITIAL_GRID,
  currentPlayer: INITIAL_PLAYER,
  winner: 0,
};

const connectMorpion = WrappedComponent => class ConnectedMorpion extends React.PureComponent {

  state = initialState;

  onClickCell = (key) => {
    const {grid, currentPlayer, winner} = this.state;

    if (winner === 0) {
      const gridCopy = [...grid];
      gridCopy[key] = currentPlayer;

      const winner = hasWon(currentPlayer, gridCopy) ? currentPlayer : 0;

      this.setState({
        grid: gridCopy,
        currentPlayer: nextPlayer(currentPlayer),
        winner,
      });

      if (winner) {
        this.props.onWin(winner);
      }
    }
  };

  onReset = () => {
    this.setState(initialState);
  };

  render() {
    return <WrappedComponent
      {...this.props}
      grid={this.state.grid}
      onClickCell={this.onClickCell}
      onClickReset={this.onReset}
      winner={this.state.winner}
    />
  }
};

export default connectMorpion;
