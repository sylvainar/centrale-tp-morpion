import React from 'react';

import MorpionGrid from "../MorpionGrid/MorpionGrid";
import Winner from "../Winner/Winner";
import Scores from "../Scores";

export default class Morpion extends React.PureComponent {
  render() {
    const {grid, onClickCell, onClickReset, winner} = this.props;
    return (
      <>
        <MorpionGrid
          grid={grid}
          onClickCell={onClickCell}
        />
        {winner !== 0 && (
          <Winner
            onClickReset={onClickReset}
            value={winner}
          />
        )}
        <Scores />
      </>
    );
  }
}
