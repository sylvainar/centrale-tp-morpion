import React from 'react';
import PropTypes from 'prop-types';

import classNames from './morpion-grid.module.css';
import MorpionCell from "../MorpionCell/MorpionCell";

const MorpionGrid = ({ grid, onClickCell }) => (
  <div className={classNames.grid}>
    {grid.map((value, index) => (
      <MorpionCell
        onClick={onClickCell}
        value={value}
        key={index}
        index={index}
      />
    ))}
  </div>
);


MorpionGrid.propTypes = {
  grid: PropTypes.arrayOf(PropTypes.number),
  onClickCell: PropTypes.func.isRequired,
};

MorpionGrid.defaultProps = {};

export default MorpionGrid;
