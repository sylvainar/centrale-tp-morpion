import React from 'react';
import PropTypes from 'prop-types';
import classNames from './morpion-cell.module.css';

class MorpionCell extends React.PureComponent {
  static propTypes = {
    index: PropTypes.number.isRequired,
    value: PropTypes.oneOf([0,1,2]).isRequired,
    onClick: PropTypes.func.isRequired,
  };

  onClick = () => {
    this.props.onClick(this.props.index);
  };

  render() {
    const {value} = this.props;
    return (
      <button onClick={this.onClick} className={classNames.button}>
        {value}
      </button>
    );
  }
}

export default MorpionCell;
