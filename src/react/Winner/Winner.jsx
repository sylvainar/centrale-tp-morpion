import React from 'react';
import PropTypes from 'prop-types';

const Winner = ({value, onClickReset}) => (
  <div>
    <p>{value} won!</p>
    <button onClick={onClickReset}>
      Reset
    </button>
  </div>
);

Winner.propTypes = {
  value: PropTypes.oneOf([0, 1, 2]).isRequired,
  onClickReset: PropTypes.func.isRequired,
};

export default Winner;
