import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

export default class Home extends React.PureComponent {
  static propTypes = {};

  static defaultProps = {};

  onClick = () => {
    this.props.history.push('/board');
  };

  render() {
    return (
      <div>
        <h1>Morpion</h1>
        <button onClick={this.onClick}>Jouer</button>
        <Link to="/rules">Rules</Link>
      </div>
    );
  }
}
