import React from 'react';
import {connect} from "react-redux";

import MorpionPresentation from "../Morpion/Morpion";
import connectMorpion from "../Morpion/morpion.connect";
import {INCREMENT_SCORE} from "../../redux/scores/actions";

const Morpion = connect(null, dispatch => ({
  onWin: (winner) => dispatch(INCREMENT_SCORE(winner)),
}))(connectMorpion(MorpionPresentation));

export default class Board extends React.PureComponent {
  static propTypes = {};

  static defaultProps = {};

  render() {
    return (
      <Morpion />
    );
  }
}
