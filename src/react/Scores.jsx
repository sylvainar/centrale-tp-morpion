import React, {Component} from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {INCREMENT_SCORE} from "../redux/scores/actions";

class Scores extends Component {
  static propTypes = {
    scores: PropTypes.objectOf(PropTypes.number).isRequired,
  };

  render() {
    const {scores} = this.props;
    return (
      <div>
        <p>Player 1: {scores[1]}
        </p>
        <p>Player 2: {scores[2]}
        </p>
      </div>
    );
  }
}

export default connect(state => ({
  scores: state.scores.currentGame,
}))(Scores);
