import * as HighScoresRepository from '../../repository/highScores';

export const ACTIONS = {
  INCREMENT_SCORE: 'SCORES/INCREMENT_SCORE',

  SET_HIGH_SCORES_START: 'SCORES/SET_HIGH_SCORES_START',
  SET_HIGH_SCORES_SUCCESS: 'SCORES/SET_HIGH_SCORES_SUCCESS',
  SET_HIGH_SCORES_FAILURE: 'SCORES/SET_HIGH_SCORES_FAILURE',
};

export const INCREMENT_SCORE = winner => ({
  type: ACTIONS.INCREMENT_SCORE,
  winner: winner,
});

export const SET_HIGH_SCORES_START = () => ({
  type: ACTIONS.SET_HIGH_SCORES_START,
});

export const SET_HIGH_SCORES_SUCCESS = highScores => ({
  type: ACTIONS.SET_HIGH_SCORES_SUCCESS,
  highScores: highScores,
});

export const SET_HIGH_SCORES_FAILURE = err => ({
  type: ACTIONS.SET_HIGH_SCORES_FAILURE,
  err,
});

export const getHighScores = () => async dispatch => {
  dispatch(SET_HIGH_SCORES_START());

  try {
    const highScores = await HighScoresRepository.getHighScores();
    dispatch(SET_HIGH_SCORES_SUCCESS(highScores));
  } catch(err) {
    dispatch(SET_HIGH_SCORES_FAILURE(err));
  }
};
