import {ACTIONS} from "./actions";
import {combineReducers} from "redux";

const initialState = {
  1: 0,
  2: 0,
};

export default combineReducers({
  currentGame: (state = initialState, action) => {
    switch (action.type) {
      case ACTIONS.INCREMENT_SCORE:
        return {
          ...state,
          [action.winner]: state[action.winner] + 1,
        };
      default:
        return state;
    }
  },

  highScores: (state = [], action) => {
    switch (action.type) {
      case ACTIONS.SET_HIGH_SCORES_SUCCESS:
        return action.highScores;
      default:
        return state;
    }
  },

  loading: (state = false, action) => {
    switch(action.type) {
      case ACTIONS.SET_HIGH_SCORES_START:
        return true;
      case ACTIONS.SET_HIGH_SCORES_SUCCESS:
      case ACTIONS.SET_HIGH_SCORES_FAILURE:
        return false;

      default:
        return state;
    }
  },
});
