export const getHighScores = () => new Promise((resolve, reject) => {
  console.log('Getting results');
  setTimeout(() => resolve([
    {name: 'Pierre', value: 20},
    {name: 'Paul', value: 30},
    {name: 'Jaques', value: 40},
  ]), 2000);
});
