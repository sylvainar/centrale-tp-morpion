import {hasWon} from "./morpion";

describe('morpion', () => {
  describe('hasWon', () => {
    it('1 wins', () => {
      const grid = [1,2,0,1,2,0,1,0,0];
      expect(hasWon(1, grid)).toBe(true);
    });

    it('2 wins', () => {
      const grid = [2,0,1,0,2,1,0,0,2];
      expect(hasWon(2, grid)).toBe(true);
    });

    it('wins if has not exactly the combination', () => {
      const grid = [2,0,1,0,2,1,0,2,2];
      expect(hasWon(2, grid)).toBe(true);
    })
  });
});

