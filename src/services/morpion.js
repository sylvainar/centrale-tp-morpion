export const INITIAL_GRID = [0,0,0,0,0,0,0,0,0];

export const INITIAL_PLAYER = 1;

export const WINNING_COMBINATIONS = [
  [true,true,true,false,false,false,false,false,false],
  [false,false,false,true,true,true,false,false,false],
  [false,false,false,false,false,false,true,true,true],
  [true,false,false,true,false,false,true,false,false],
  [false,true,false,false,true,false,false,true,false],
  [false,false,true,false,false,true,false,false,true],
  [true,false,false,false,true,false,false,false,true],
  [false,false,true,false,true,false,true,false,false],
];

export const hasWon = (player, grid) => WINNING_COMBINATIONS
  .some(combination => combination
    .every((value, index) => value === false || value === (grid[index] === player)));

export const nextPlayer = currentPlayer => currentPlayer === 1 ? 2 : 1;
